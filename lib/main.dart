import 'package:flutter/material.dart';

import 'package:your_news_app/src/news_bloc.dart';
import 'package:your_news_app/src/news_store.dart';
import 'package:your_news_app/app_context.dart';
import 'package:your_news_app/src/my_home_page.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  final newsBloc = NewsBloc();
  final newsStore = NewsStore();

  @override
  Widget build(BuildContext context) {
    return AppContext(
      newsBloc: newsBloc,
      newsStore: newsStore,
      child: new MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Your News App',
        theme: new ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: new MyHomePage(
          title: 'Flutter Demo Home Page',
        ),
      ),
    );
  }
}