import 'dart:collection';

import 'package:flutter/material.dart';

import 'package:your_news_app/src/newsfilter/news_filter.dart';
import 'package:your_news_app/src/serializers/news_source.dart';
import 'package:your_news_app/app_context.dart';

class NewsFilterScreen extends StatelessWidget {
  final NewsFilter newsFilter;

  NewsFilterScreen({Key key, this.newsFilter}) : super(key: key) {
    print('Screen constructor');
  }

  @override
  Widget build(BuildContext context) {
    print('Screen build');
    return Scaffold(
      appBar: AppBar(
        title: Text('Customize News Filter'),
      ),
      body: NewsFilterPage(newsFilter: newsFilter),
    );
  }
}

class NewsFilterPage extends StatefulWidget {
  final NewsFilter newsFilter;

  NewsFilterPage({this.newsFilter}) {
    print('Stateful Widget constructor');
  }

  @override
  _NewsFilterState createState() => new _NewsFilterState();
}

class _NewsFilterState extends State<NewsFilterPage> {
  final _formKey = GlobalKey<FormState>();

  Map<String, bool> newsSourcesCheckboxes;
  List<String> qList;

  List<TextEditingController> keywordControllers = List<TextEditingController>();
  List<Widget> keywords = <Widget>[];
  Widget keywordGroup;
  Widget submitButton;
  Form form;

  @override
  void initState() {
    super.initState();

    const double padding = 12.0;

    qList = widget.newsFilter.q;

    newsSourcesCheckboxes = Map<String, bool>();
    widget.newsFilter.newsSourcesIds.forEach((String id) {
      newsSourcesCheckboxes[id] = true;
    });

    // Keywords
    qList.forEach((String str) {
      TextEditingController controller = TextEditingController(
        text: str,
      );
      keywordControllers.add(controller);
      keywords.add(Padding(
        padding:
        const EdgeInsets.only(left: padding, right: padding, top: padding),
        child: TextFormField(
          controller: controller,
          validator: (value) {
            if (value.isEmpty) {
              return 'Please enter some text';
            }
          },
        ),
      ));
    });

    keywordGroup = Column(
      children: keywords,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(children: [
        Text('Keywords'),
        keywordGroup,
        _getSubmitButton(),
        _getNewsSourcesList(),
      ]),
    );
  }

  Widget _getSubmitButton() {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: RaisedButton(
        onPressed: () {
          if (_formKey.currentState.validate()) {
            Scaffold
                .of(context)
                .showSnackBar(SnackBar(content: Text('Processing Data')));

            List<String> q = <String>[];
            keywordControllers.forEach((controller) {
              q.add(controller.text);
            });
            widget.newsFilter.q = q;

            AppContext.of(context).newsStore.updateNewsFilter(widget.newsFilter);
            print('NewsFilters updated');
          }
        },
        child: Text('Submit'),
      ),
    );
  }

  Widget _getNewsSourcesList() {
    return Expanded(
      child: StreamBuilder<UnmodifiableListView<NewsSource>>(
        stream: AppContext.of(context).newsBloc.newsSources,
        initialData: UnmodifiableListView<NewsSource>([]),
        builder: (context, snapshot) => ListView(
          children: snapshot.data.map(_buildNewsSourceWidget).toList(),
        ),
      ),
    );
  }

  Widget _buildNewsSourceWidget(NewsSource ns) {
    return Row(children: [
      Checkbox(
          value: newsSourcesCheckboxes[ns.id] ?? false,
          onChanged: (bool b) {
            setState(() {
              newsSourcesCheckboxes[ns.id] = b;
            });
          }),
      Text(ns.name),
    ]);
  }
}
