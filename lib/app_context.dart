import 'package:flutter/material.dart';

import 'package:your_news_app/src/news_bloc.dart';
import 'package:your_news_app/src/news_store.dart';
import 'package:your_news_app/src/newsfilter/news_filter.dart';

class AppContext extends InheritedWidget {
  final NewsBloc newsBloc;
  final NewsStore newsStore;
  NewsFilter currentNewsFilter;

  AppContext(
      {Key key,
      @required this.newsBloc,
      @required this.newsStore,
      @required Widget child})
      : super(key: key, child: child);

  static AppContext of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(AppContext);
  }

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }
}
