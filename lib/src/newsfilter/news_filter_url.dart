/// Class for building a news api URL
/// Can't mix category and sources
/// Can't mix country and sources
class NewsFilterURL {
  static final String _newsApiKey = 'apiKey=8bd273e8ac3944568b88341b540e2194';
  static final String _baseURL = 'http://newsapi.org/v2/';
  static final String _topHeadlines = 'top-headlines';

  // You can't mix country and sources
  Map<String, dynamic> _parameters;

  StringBuffer buffer = StringBuffer();

  NewsFilterURL() {
    _parameters = {
      'country': null,
      'category': null,
      'sources': [],
      'q': [], // keyword or phrase
      'pageSize': null,
      'page': null,
    };

    buffer.write(_baseURL);
    buffer.write(_topHeadlines);
  }

  // TODO : do this better
  @override
  String toString() {
    bool first = true;

    _parameters.forEach((String key, dynamic value) {
      if (value != null) {
        String add = first ? '?' : '&';

        switch (key) {
          case 'language':
            buffer.write('${add}language=${value}');
            first = false;
            break;
          case 'country':
            buffer.write('${add}country=${value}');
            first = false;
            break;
          case 'category':
            buffer.write('${add}category=${value}');
            first = false;
            break;
          case 'sources':
            String list = processArray(value);
            if (list != null) {
              buffer.write('${add}sources=${list}');
              first = false;
            }
            break;
          case 'q':
            String list = processArray(value);
            if (list != null) {
              buffer.write('${add}q=${list}');
              first = false;
            }
            break;
          case 'pageSize':
            buffer.write('${add}pageSize=${value.toString()}');
            first = false;
            break;
          case 'page':
            buffer.write('${add}page=${value.toString()}');
            first = false;
            break;
        }
      }
    });

    String str = first ? '?' : '&';
    buffer.write(str);

    buffer.write(_newsApiKey);
    return buffer.toString();
  }

  String processArray(dynamic obj) {
    if (obj == null || List.from(obj).isEmpty) {
      return null;
    }
    List<String> list = obj;
    String str = '';
    list.forEach((s) => str += s + '&');
    str = str.substring(0, str.length - 1);
    return str;
  }

  setLanguage(String language) {
    this._parameters['language'] = language;
  }

  setCountry(String country) {
    this._parameters['country'] = country;
  }

  setCategory(String category) {
    this._parameters['category'] = category;
  }

  setSources(List<String> sources) {
    this._parameters['sources'] = sources;
  }

  // There's probably a better way to do this?
  addSource(String source) {
    List<String> list = this._parameters['sources'];
    list.add(source);
    this._parameters['sources'] = list;
  }

  setQ(List<String> q) {
    this._parameters['q'] = q;
  }

  // There's probably a better way to do this?
  addKeyword(String keyword) {
    List<String> list = this._parameters['q'];
    list.add(keyword);
    this._parameters['q'] = list;
  }

  setPageSize(String pageSize) {
    this._parameters['pageSize'];
  }

  setPage(String page) {
    this._parameters['page'];
  }
}