import 'package:collection/collection.dart';

import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:quiver/core.dart';
import 'package:your_news_app/src/newsfilter/news_filter_url.dart';

/// One of the following parameters must be set: sources, q, language, country, category
class NewsFilter {
  DocumentReference docRef;
  List<String> q;
  List<String> newsSourcesIds;
  String language;
  String country;
  String category;
  int pageSize;
  int page;

  /// Construct a basic NewsFilter.
  /// Automatically sets country to "en" to avoid news api error
  NewsFilter({this.q, this.newsSourcesIds}) {
    this.language = "en";
  }

  NewsFilter.basic({this.q}) : assert(q != null);

  Map<String, dynamic> getMap() {
    return <String, dynamic>{
      'q': q,
      'newsSourcesIds': newsSourcesIds,
      'language': language,
      'country': country,
      'category': category,
      'pageSize': pageSize,
      'page': page,
    };
  }

  @override
  String toString() {
    return 'NewsFilter:: keyword: ${q.first}';
  }

  bool operator ==(nf) {
    Function eq = const ListEquality().equals;
    return nf is NewsFilter &&
        eq(q, nf.q) &&
        eq(newsSourcesIds, nf.newsSourcesIds);
  }

  // TODO: I think we can have either a news source ID or a country, not both
  // but that does mean news source IDs could be null
  int get hashCode {
    return hash2(q.first, newsSourcesIds.first);
  }

  NewsFilterURL buildNewsFilterUrl() {
    NewsFilterURL nfUrl = NewsFilterURL();
    getMap().forEach((String key, dynamic value) {
      switch (key) {
        case 'q':
          List<String> list = value;
          nfUrl.setQ(list);
          break;
        case 'newsSourcesIds':
          List<String> list = value;
          nfUrl.setSources(list);
          break;
        case 'language':
          nfUrl.setLanguage(value as String);
          break;
        case 'country':
          nfUrl.setCountry(value as String);
          break;
        case 'category':
          nfUrl.setCategory(value as String);
          break;
        case 'pageSize':
          nfUrl.setPageSize(value as String);
          break;
        case 'page':
          nfUrl.setPage(value as String);
          break;
      }
    });
    return nfUrl;
  }

  // STATIC FINAL DATA
  static final List<String> categories = [
    "business",
    "entertainment",
    "general",
    "health",
    "science",
    "sports",
    "technology",
  ];

  static final Map<String, String> countries = {
    "China": "cn",
    "United Kingdom": "gb",
    "France": "fr",
    "United States": "us",
    "Japan": "jp",
    "Germay": "de",
    "Canada": "ca",
  };
}

NewsFilter buildNewsFilter(DocumentReference docRef, Map<String, dynamic> map) {
  NewsFilter nf = NewsFilter();
  nf.docRef = docRef;
  map.forEach((key, value) {
    switch (key) {
      case ('q'):
        nf.q = [];
        for (String x in value) {
          nf.q.add(x);
        }
        break;
      case ('newsSourcesIds'):
        nf.newsSourcesIds = [];
        for (String x in value) {
          nf.newsSourcesIds.add(x);
        }
        break;
    }
  });
  if (nf.q != null) {
    return nf;
  } else {
    return null;
  }
}
