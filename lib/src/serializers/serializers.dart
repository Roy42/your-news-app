library serializers;

import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:your_news_app/src/serializers/article.dart';
import 'package:your_news_app/src/serializers/source.dart';
import 'package:your_news_app/src/serializers/news_source.dart';

part 'serializers.g.dart';

/// Example of how to use built_value serialization.
///
/// Declare a top level [Serializers] field called serializers. Annotate it
/// with [SerializersFor] and provide a `const` `List` of types you want to
/// be serializable.
///
/// The built_value code generator will provide the implementation. It will
/// contain serializers for all the types asked for explicitly plus all the
/// types needed transitively via fields.
///
/// You usually only need to do this once per project.
/// Run 'flutter packages pub run build_runner build' in the flutter console
@SerializersFor(const [
  Article,
  Source,
  NewsSource
])
Serializers serializers = _$serializers;

Serializers standardSerializers = (serializers.toBuilder()
  ..addPlugin(new StandardJsonPlugin())).build();