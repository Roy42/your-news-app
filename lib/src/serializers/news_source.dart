import 'dart:convert' as json;
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:your_news_app/src/serializers/serializers.dart';

part 'news_source.g.dart';

/// Run 'flutter packages pub run build_runner build' in the flutter console
/// to generate the file article.g.dart
/// Uses: https://github.com/google/built_value.dart
abstract class NewsSource implements Built<NewsSource, NewsSourceBuilder> {
  static Serializer<NewsSource> get serializer => _$newsSourceSerializer;

  String get id;
  String get name;
  String get description;
  String get url;
  String get category;
  String get language;
  String get country;

  NewsSource._();
  factory NewsSource([updates(NewsSourceBuilder b)]) = _$NewsSource;
}
/// Expects the JSON passed to it to contain a key "sources" which is a list of news sources
List<NewsSource> parseSources(String jsonStr) {
  final parsed = json.jsonDecode(jsonStr);
  List<NewsSource> sources = <NewsSource>[];
  for (dynamic source in parsed['sources']) {
    NewsSource ns = standardSerializers.deserializeWith(NewsSource.serializer, source);
    sources.add(ns);
  }
  return sources;
}
