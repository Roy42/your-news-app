import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:your_news_app/src/serializers/serializers.dart';

part 'source.g.dart';

abstract class Source implements Built<Source, SourceBuilder> {
  static Serializer<Source> get serializer => _$sourceSerializer;

  // Not a numerical ID, more like a unique friendly key
  // e.g. BBC News is bbc-news
  @nullable
  String get id;
  @nullable
  String get name;

  Source._();
  factory Source([updates(SourceBuilder b)]) = _$Source;
}