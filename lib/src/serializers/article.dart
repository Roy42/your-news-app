import 'dart:convert' as json;
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:your_news_app/src/serializers/serializers.dart';
import 'package:your_news_app/src/serializers/source.dart';

part 'article.g.dart';

/// Run 'flutter packages pub run build_runner build' in the flutter console
/// to generate the file article.g.dart
/// Uses: https://github.com/google/built_value.dart
abstract class Article implements Built<Article, ArticleBuilder> {
  static Serializer<Article> get serializer => _$articleSerializer;

  @nullable
  Source get source;

  @nullable
  String get author;
  String get title;
  @nullable
  String get description;
  @nullable
  String get url;
  @nullable
  String get urlToImage;
  @nullable
  String get publishedAt;

  Article._();
  factory Article([updates(ArticleBuilder b)]) = _$Article;
}

/// Expects a list of IDs in JSON form
List<int> parseTopStories(String jsonStr) {
  final parsed = json.jsonDecode(jsonStr);
  final listOfIds = List<int>.from(parsed);
  return listOfIds;
  }

/// Expects a map of string to dynamic
//Article parseArticle(String jsonStr) {
//  final parsed = json.jsonDecode(jsonStr);
//  Article article = standardSerializers.deserializeWith(Article.serializer, parsed);
//  return article;
//}

/// Expects the JSON passed to it to contain a key "articles" which is a list of articles
List<Article> parseArticles(String jsonStr) {
  final parsed = json.jsonDecode(jsonStr);
  List<Article> articles = <Article>[];
  for (dynamic article in parsed['articles']) {
    Article art = standardSerializers.deserializeWith(Article.serializer, article);
    articles.add(art);
  }
  return articles;
}