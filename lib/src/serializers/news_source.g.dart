// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_source.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line
// ignore_for_file: annotate_overrides
// ignore_for_file: avoid_annotating_with_dynamic
// ignore_for_file: avoid_returning_this
// ignore_for_file: omit_local_variable_types
// ignore_for_file: prefer_expression_function_bodies
// ignore_for_file: sort_constructors_first

Serializer<NewsSource> _$newsSourceSerializer = new _$NewsSourceSerializer();

class _$NewsSourceSerializer implements StructuredSerializer<NewsSource> {
  @override
  final Iterable<Type> types = const [NewsSource, _$NewsSource];
  @override
  final String wireName = 'NewsSource';

  @override
  Iterable serialize(Serializers serializers, NewsSource object,
      {FullType specifiedType: FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'description',
      serializers.serialize(object.description,
          specifiedType: const FullType(String)),
      'url',
      serializers.serialize(object.url, specifiedType: const FullType(String)),
      'category',
      serializers.serialize(object.category,
          specifiedType: const FullType(String)),
      'language',
      serializers.serialize(object.language,
          specifiedType: const FullType(String)),
      'country',
      serializers.serialize(object.country,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  NewsSource deserialize(Serializers serializers, Iterable serialized,
      {FullType specifiedType: FullType.unspecified}) {
    final result = new NewsSourceBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'url':
          result.url = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'category':
          result.category = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'language':
          result.language = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'country':
          result.country = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$NewsSource extends NewsSource {
  @override
  final String id;
  @override
  final String name;
  @override
  final String description;
  @override
  final String url;
  @override
  final String category;
  @override
  final String language;
  @override
  final String country;

  factory _$NewsSource([void updates(NewsSourceBuilder b)]) =>
      (new NewsSourceBuilder()..update(updates)).build();

  _$NewsSource._(
      {this.id,
      this.name,
      this.description,
      this.url,
      this.category,
      this.language,
      this.country})
      : super._() {
    if (id == null) throw new BuiltValueNullFieldError('NewsSource', 'id');
    if (name == null) throw new BuiltValueNullFieldError('NewsSource', 'name');
    if (description == null)
      throw new BuiltValueNullFieldError('NewsSource', 'description');
    if (url == null) throw new BuiltValueNullFieldError('NewsSource', 'url');
    if (category == null)
      throw new BuiltValueNullFieldError('NewsSource', 'category');
    if (language == null)
      throw new BuiltValueNullFieldError('NewsSource', 'language');
    if (country == null)
      throw new BuiltValueNullFieldError('NewsSource', 'country');
  }

  @override
  NewsSource rebuild(void updates(NewsSourceBuilder b)) =>
      (toBuilder()..update(updates)).build();

  @override
  NewsSourceBuilder toBuilder() => new NewsSourceBuilder()..replace(this);

  @override
  bool operator ==(dynamic other) {
    if (identical(other, this)) return true;
    if (other is! NewsSource) return false;
    return id == other.id &&
        name == other.name &&
        description == other.description &&
        url == other.url &&
        category == other.category &&
        language == other.language &&
        country == other.country;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, id.hashCode), name.hashCode),
                        description.hashCode),
                    url.hashCode),
                category.hashCode),
            language.hashCode),
        country.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('NewsSource')
          ..add('id', id)
          ..add('name', name)
          ..add('description', description)
          ..add('url', url)
          ..add('category', category)
          ..add('language', language)
          ..add('country', country))
        .toString();
  }
}

class NewsSourceBuilder implements Builder<NewsSource, NewsSourceBuilder> {
  _$NewsSource _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  String _url;
  String get url => _$this._url;
  set url(String url) => _$this._url = url;

  String _category;
  String get category => _$this._category;
  set category(String category) => _$this._category = category;

  String _language;
  String get language => _$this._language;
  set language(String language) => _$this._language = language;

  String _country;
  String get country => _$this._country;
  set country(String country) => _$this._country = country;

  NewsSourceBuilder();

  NewsSourceBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _name = _$v.name;
      _description = _$v.description;
      _url = _$v.url;
      _category = _$v.category;
      _language = _$v.language;
      _country = _$v.country;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(NewsSource other) {
    if (other == null) throw new ArgumentError.notNull('other');
    _$v = other as _$NewsSource;
  }

  @override
  void update(void updates(NewsSourceBuilder b)) {
    if (updates != null) updates(this);
  }

  @override
  _$NewsSource build() {
    final _$result = _$v ??
        new _$NewsSource._(
            id: id,
            name: name,
            description: description,
            url: url,
            category: category,
            language: language,
            country: country);
    replace(_$result);
    return _$result;
  }
}
