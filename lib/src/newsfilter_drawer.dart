import 'dart:collection';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:your_news_app/app_context.dart';
import 'package:your_news_app/src/newsfilter/news_filter.dart';
import 'package:your_news_app/view/news_filter_screen.dart';

class NewsFilterDrawer extends StatefulWidget {
  final Stream<QuerySnapshot> stream;
  final Function() notifyParent;
  final Map<String, bool> checkboxCheckedStates = HashMap();
  NewsFilter currentNewsFilter;

  NewsFilterDrawer({@required this.stream, @required this.notifyParent});

  @override
  _NewsFilterDrawerState createState() => new _NewsFilterDrawerState();
}

class _NewsFilterDrawerState extends State<NewsFilterDrawer> {

  @override
  Widget build(BuildContext context) {
    return Drawer(child: _getDrawerTilesBuilder());
  }

  DrawerHeader _getDrawerHeader() {
    return DrawerHeader(
      child: Center(child: Text('News Filters')),
      decoration: BoxDecoration(
        color: Colors.lightBlueAccent,
      ),
    );
  }

  StreamBuilder<QuerySnapshot> _getDrawerTilesBuilder() {
    return StreamBuilder<QuerySnapshot>(
        stream: widget.stream,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          List<Widget> list = <Widget>[];
          list.add(_getDrawerHeader());
          List<Widget> filterTiles = <Widget>[];
          if (snapshot.connectionState != ConnectionState.active) {
            list.add(Text('Loading...'));
          } else {
            snapshot.data.documents.forEach((DocumentSnapshot ds) {
              // Build News Filter object
              NewsFilter nf = buildNewsFilter(ds.reference, ds.data);
              bool checked = nf == widget.currentNewsFilter ? true : false;

              // Checkbox
              Checkbox checkbox = Checkbox(
                value: checked,
                onChanged: (bool b) {
                  setState(() {
                    if (b) {
                      AppContext.of(context).newsBloc.updateNewsFilter.add(nf);
                      widget.currentNewsFilter = nf;
                      print(widget.currentNewsFilter.q.first);
//                      Navigator.pop(context);
//                      widget.notifyParent();
                    }
                  });
                },
              );

              // IconButton
              IconButton editButton = IconButton(
                  icon: Icon(Icons.edit),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => NewsFilterScreen(
                            newsFilter: nf,
                          )),
                    );
                  });

              // News Filter Keyword
              Widget keyword = Expanded(
                  child: Text(
                    nf.q.first,
                    style: TextStyle(
                      fontSize: 18.0,
                    ),
                  ));

              list.add(Row(
              children: <Widget>[
                checkbox,
                keyword,
                editButton,
              ],
                ));
            }); // end of foreach on documents
          }
          return ListView(
            children: list,
          );
        });
  }
}

/*
Opacity(
opacity: 0.3,
child: Container(
decoration: BoxDecoration(
color: Colors.blue,
),
),
),
*/
