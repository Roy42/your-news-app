import 'dart:async';
import 'dart:collection';

import 'package:http/http.dart' as http;
import 'package:rxdart/rxdart.dart';

import 'package:flutter/material.dart';

import 'package:your_news_app/src/newsfilter/news_filter.dart';
import 'package:your_news_app/src/serializers/article.dart';
import 'package:your_news_app/src/serializers/news_source.dart';

/// Bloc for retrieving news items from https://newsapi.org/
class NewsBloc {

  // TODO: we get a certificate exception when we use https. Try and find a fix. For now we'll use http

  final String _newsApi = 'https://newsapi.org/v2/';
  final String _everything = 'everything?';
  final String _topic = 'q=';
  final String _pageSize = '&pageSize=1';
  final String _sourceBBC = '&source=bbc-news';
  static final String _newsApiKey = '8bd273e8ac3944568b88341b540e2194';
  final String _testUrl = 'http://newsapi.org/v2/everything?sources=bbc-news&pageSize=100&language=en';
  NewsFilter currentNewsFilter;
  final String _sourcesUrl = 'http://newsapi.org/v2/sources?apiKey=$_newsApiKey';

  var _articles = <Article>[];
  var _sources = <NewsSource>[];

  Stream<UnmodifiableListView<Article>> get articles => _articleSubject.stream;
  final _articleSubject = BehaviorSubject<UnmodifiableListView<Article>>();

  Stream<UnmodifiableListView<NewsSource>> get newsSources => _newsSourcesSubject.stream;
  final _newsSourcesSubject = BehaviorSubject<UnmodifiableListView<NewsSource>>();

  Stream<bool> get isLoading => _isLoadingSubject.stream;
  final _isLoadingSubject = BehaviorSubject<bool>(seedValue: false);

  Sink<bool> get update => _updateController.sink;
  final _updateController = StreamController<bool>();

  Sink<NewsFilter> get updateNewsFilter => _updateNewsFilterController.sink;
  final _updateNewsFilterController = StreamController<NewsFilter>();

  NewsBloc() {
    init();

    currentNewsFilter = NewsFilter();

    print('NewsBloc initiated');

    // Set listeners
    _updateController.stream.listen((b) {
      if (b) {
        getArticlesAndUpdate();
      }
    });

    _updateNewsFilterController.stream.listen((NewsFilter nf) {
      currentNewsFilter = nf;
      print('News Filter set');
    });
  }

  init() {
    _isLoadingSubject.add(true);
    getArticlesAndUpdate();
    getNewsSourcesAndUpdate();
    _isLoadingSubject.add(false);
  }

  getArticlesAndUpdate() async {
    updateArticles();
    print('Articles updated');
  }

  Future<Null> getNewsSourcesAndUpdate() async {
    final futureSources = getNewsSources();
    await futureSources.then((sources) => _sources = sources);
    _newsSourcesSubject.add(UnmodifiableListView(_sources));
    print('Sources updated');
  }

  Future<List<NewsSource>> getNewsSources() async {
    final sourcesRes = await http.get(_sourcesUrl);
    if (sourcesRes.statusCode == 200) {
      return parseSources(sourcesRes.body);
    }
  }

  Future<Null> updateArticles() async {
    final futureArticle = getArticles(currentNewsFilter.buildNewsFilterUrl().toString());
    await futureArticle.then((articles) => _articles = articles);
    _articleSubject.add(UnmodifiableListView(_articles));
  }

  Future<List<Article>> getArticles(String articleUrl) async {
    print('NEWS API GET: ${articleUrl}');
    final articleRes = await http.get(
      Uri.encodeFull(
          articleUrl
      ),
      headers: {
        "Accept": "application/json",
        "X-Api-Key" : _newsApiKey,
      }
    );
    if (articleRes.statusCode == 200) {
      return parseArticles(articleRes.body);
    }
  }
}