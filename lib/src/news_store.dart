import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:your_news_app/src/newsfilter/news_filter.dart';

/// Persists and retrieves items from the Firestore Database
class NewsStore {

  final Firestore firestore = Firestore.instance;
  final String newsFilters = "newsfilters";

  factory NewsStore() {
    return NewsStore._internal();
  }

  NewsStore._internal();

  bool persistNewsFilter(NewsFilter nf) {
    Future<DocumentReference> futureDocRef = firestore.collection(newsFilters).add(nf.getMap());
    futureDocRef.catchError(() {
      return false;
    });
    return true;
  }

  Stream<QuerySnapshot> getFutureNewsFilters() {
    return firestore.collection(newsFilters).snapshots();
  }

  List<NewsFilter> getAllNewsFilters() {
    List<NewsFilter> filters = <NewsFilter>[];
    firestore.collection(newsFilters).getDocuments().then((QuerySnapshot qs) {
      qs.documents.forEach((DocumentSnapshot ds) {
        filters.add(buildNewsFilter(ds.reference, ds.data));
      });
    });
    return filters;
  }

  void updateNewsFilter(NewsFilter nf) {
    firestore.runTransaction((transaction) async {
      await transaction.update(nf.docRef, nf.getMap());
    });

  }
}