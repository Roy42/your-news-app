import 'dart:collection';
import 'dart:async';

import 'package:flutter/material.dart';

import 'package:your_news_app/src/serializers/article.dart';
import 'package:your_news_app/app_context.dart';
import 'package:your_news_app/src/newsfilter_drawer.dart';

class MyHomePage extends StatefulWidget {
  final String title;

  MyHomePage({Key key, @required this.title}) : super(key: key);

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Map<String, bool> checkboxCheckedStates = Map<String, bool>();

  refresh() {
    setState(() {
      _handleRefresh();
      print('News List refreshed');
    });
  }

  Future<Null> _handleRefresh() async {
    await AppContext.of(context).newsBloc.update.add(true);
    return null;
  }

  Widget _buildDrawer() {
    return NewsFilterDrawer(stream: AppContext.of(context).newsStore.getFutureNewsFilters(), notifyParent: refresh,);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: _buildDrawer(),
      body: RefreshIndicator(
        onRefresh: _handleRefresh,
        child: StreamBuilder<UnmodifiableListView<Article>>(
            stream: AppContext.of(context).newsBloc.articles,
            initialData: UnmodifiableListView<Article>([]),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.active) {
                return ListView(
                  children: snapshot.data.map(_buildItem).toList(),
                );
              } else {
                return Text('Loading...');
              }
            }),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: 0,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            title: Text('Display'),
            icon: Icon(Icons.edit),
          ),
          BottomNavigationBarItem(
            title: Text('Other Thing'),
            icon: Icon(Icons.build),
          ),
        ],
      ),
    );
  }

  Widget _buildItem(Article article) {
    ImageProvider image = AssetImage('assets/news-placeholder.png');
    if (article.urlToImage != null) {
      image = NetworkImage(
        article.urlToImage,
      );
    }
    return Padding(
      key: Key(article.title),
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          Container(
              width: 400.0,
              height: 200.0,
              decoration: BoxDecoration(
                image: DecorationImage(image: image),
              )),
          Padding(
            child: Text(article.title,
                style: TextStyle(color: Colors.black, fontFamily: "Patua One")),
            padding: EdgeInsets.only(top: 5.0),
          ),
        ],
      ),
    );
  }
}