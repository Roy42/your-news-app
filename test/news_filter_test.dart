import 'package:test/test.dart';

import 'package:your_news_app/src/newsfilter/news_filter.dart';
import 'package:your_news_app/src/newsfilter/news_filter_url.dart';

void main() {
  test('Test building a news fitler from a map', () {
    Map<String, dynamic> map = <String, dynamic>{
      'q' : ['robot','robots', 'robotics'],
      'newsSourcesIds' : ['bbc-news', 'msnbc'],
    };

    NewsFilter nf = buildNewsFilter(null ,map);
    expect(nf.q.first, equals('robot'));
    expect(nf.q[1], equals('robots'));
    expect(nf.q[2], equals('robotics'));
    expect(nf.newsSourcesIds.first, equals('bbc-news'));
    expect(nf.newsSourcesIds.last, equals('msnbc'));
  });

  test('Build NewsFilterURL, base test', (){
    NewsFilter nf = NewsFilter();
    NewsFilterURL nfUrl = nf.buildNewsFilterUrl();

    String expectedUrl = "http://newsapi.org/v2/top-headlines?language=en&apiKey=8bd273e8ac3944568b88341b540e2194";

    expect(nfUrl.toString(), expectedUrl);
  });
}