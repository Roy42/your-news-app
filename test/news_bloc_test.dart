import 'dart:async';
import 'dart:collection';

import 'package:test/test.dart';
import 'package:your_news_app/src/news_bloc.dart';
import 'package:your_news_app/src/serializers/article.dart';
import 'package:your_news_app/src/serializers/news_source.dart';

void main() {

  test("Test get one news source from NewsBloc", () async {
    NewsBloc bloc = NewsBloc();

    Stream<UnmodifiableListView<NewsSource>> stream = bloc.newsSources;

    await for (var value in stream) {
      expect(value, isNotEmpty);
    }
  });

}