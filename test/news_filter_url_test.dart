import 'package:test/test.dart';

import 'package:your_news_app/src/newsfilter/news_filter_url.dart';

void main() {

  test("Base case, construct top headlines url with no filters", () {
    NewsFilterURL nfUrl = NewsFilterURL();
    String expectedUrl = "http://newsapi.org/v2/top-headlines?apiKey=8bd273e8ac3944568b88341b540e2194";
    expect(nfUrl.toString(), equals(expectedUrl));
  });

  test('Construct standard top headlines news filter', () {
    NewsFilterURL nfUrl = NewsFilterURL();

    nfUrl.setSources(['msnbc', 'bbc-news', 'google-news']);
    nfUrl.setQ(['robot', 'robots', 'robotics']);

    String expectedUrl = 'http://newsapi.org/v2/top-headlines?sources=msnbc&bbc-news&google-news&q=robot&robots&robotics&apiKey=8bd273e8ac3944568b88341b540e2194';

    expect(nfUrl.toString(), equals(expectedUrl));
  });
}
