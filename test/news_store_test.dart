import 'package:test/test.dart';

import 'package:your_news_app/src/news_store.dart';
import 'package:your_news_app/src/newsfilter/news_filter.dart';

void main() {

  test("Test get news filters", () {
    final newsStore = NewsStore();
    List<NewsFilter> filters = newsStore.getAllNewsFilters();
    expect(filters, isNotEmpty);
  });

}