import 'dart:convert';
import 'dart:async';

import 'package:test/test.dart';
import 'package:http/http.dart' as http;
import 'package:your_news_app/src/serializers/article.dart';

void main() {

  test("Get single article", () async {
    const url = 'https://newsapi.org/v2/everything?sources=bbc-news&pageSize=1&language=en&apiKey=8bd273e8ac3944568b88341b540e2194';
    final res = await http.get(url);

    if (res.statusCode == 200) {
      List<Article> articles = parseArticles(res.body);
      expect(articles, hasLength(1));
      expect(articles.first.source.name, 'BBC News');
      expect(articles.first.title, isNotNull);
      expect(articles.first.url, isNotNull);
      expect(articles.first.urlToImage, isNotNull);
    }

  });

}