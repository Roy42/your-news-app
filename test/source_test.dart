import 'package:test/test.dart';
import 'package:http/http.dart' as http;
import 'package:your_news_app/src/serializers/news_source.dart';

void main() {

  test("Get single article", () async {
    const url = 'https://newsapi.org/v2/sources?apiKey=8bd273e8ac3944568b88341b540e2194';
    final res = await http.get(url);

    if (res.statusCode == 200) {
      List<NewsSource> articles = parseSources(res.body);
      expect(articles, isNotEmpty);
      expect(articles.first.id, isNotNull);
    }
  });
}